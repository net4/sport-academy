﻿using Competition_Manager.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Competition_Manager
{
    class Program
    {

        //Method to create Coach object and insert in into a database
        public static void AddCoach(Coach coach)
        {
            using (CoachAthleteDbContext coachAthleteDbContext = new CoachAthleteDbContext())
            {
                coachAthleteDbContext.Coaches.Add(new Coach() { FirstName = coach.FirstName, LastName = coach.LastName, Age = coach.Age, Expertise = coach.Expertise });
                coachAthleteDbContext.SaveChanges();
            }

        }
        //Method to create Athlete object and insert in into a database
        public static void AddAthlete(Athlete athlete)
        {
            using (CoachAthleteDbContext coachAthleteDbContext = new CoachAthleteDbContext())
            {
                coachAthleteDbContext.Athletes.Add(new Athlete(){FirstName=athlete.FirstName,LastName=athlete.LastName,Age=athlete.Age,Sport=athlete.Sport,CoachId=athlete.CoachId});
                coachAthleteDbContext.SaveChanges();
            }
        }
        //Method for creating sponsors
        public static void AddSponsor(Sponsor sponsor)
        {
            using (CoachAthleteDbContext coachAthleteDbContext = new CoachAthleteDbContext())
            {
                coachAthleteDbContext.Sponsors.Add(new Sponsor() { Name = sponsor.Name, Salary=sponsor.Salary});
                coachAthleteDbContext.SaveChanges();
            }

        }
        //Method to view all Athletes
        public static void AllAthletes()
        {
            using (CoachAthleteDbContext coachAthleteDbContext = new CoachAthleteDbContext())
            {
                var allAthletes = coachAthleteDbContext.Athletes.ToList();
                foreach (Athlete athlete in allAthletes)
                {
                    Console.WriteLine($"Name: {athlete.FirstName} {athlete.LastName}\n Coached by: {athlete.Coach.FirstName} {athlete.Coach.LastName}");
                }
            }
        }
        //Method for serializing athletes
        public static void serializeAthletes()
        {
            using (CoachAthleteDbContext coachAthleteDbContext = new CoachAthleteDbContext())
            {
                var allAthletes = coachAthleteDbContext.Athletes
                                .Include(athlete => athlete.Coach)
                                .Include(athlete => athlete.sponsorAthletes)
                                .ThenInclude(sponsorAthlete => sponsorAthlete.sponsor)
                                .ToList();

                foreach (Athlete athlete in allAthletes)
                {
                    string jsonAthlete = JsonConvert.SerializeObject(athlete, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    Console.WriteLine(jsonAthlete);
                }
            }
        }

        static void Main(string[] args)
        {
            //AddCoach(new Coach() {FirstName="Jens", LastName="Stoltenberg", Age=31, Expertise="Yoga"});
            //AddAthlete(new Athlete(){FirstName ="Steven",LastName ="James",Age=16,Sport="Snooker", CoachId=3});
            //AddSponsor(new Sponsor() { Name = "McDonalds", Salary = 50 });
            AllAthletes();
        }
    }
}

