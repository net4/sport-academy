﻿using Competition_Manager.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    public class CoachResume
    {
        public int Id { get; set; }
        public string Nationality { get; set; }
        public string Biography { get; set; }

        public int CoachId { get; set; }
        public Coach Coach { get; set; }
    }
}
