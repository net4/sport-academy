﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager.Models
{
    public class Coach
    {
        //Setting initial properties
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Expertise { get; set; }
        public ICollection<Athlete> Athletes { get; set; }
        public CoachResume Resume { get; set; }
    }
}
