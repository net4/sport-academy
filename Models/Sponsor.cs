﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    public class Sponsor
    {
        //Setting properties
        public int Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }
        public ICollection<SponsorAthlete> sponsorAthletes { get; set; }

    }
}
