﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager.Models
{
    public class CoachAthleteDbContext : DbContext
    {
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Athlete> Athletes { get; set; }
        public DbSet<Sponsor> Sponsors { get; set; }
        public DbSet<SponsorAthlete> SponsorAthletes { get; set; }
        public DbSet<CoachResume> CoachResumes { get; set; }

        protected override void OnConfiguring (DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=PC7362\\SQLEXPRESS; Initial Catalog=SportAcademyDB; Integrated Security=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SponsorAthlete>().HasKey(sa => new { sa.SponsorId, sa.AthleteId });
        }
    }
}
