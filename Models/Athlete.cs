﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager.Models
{
    public class Athlete
    {
        //Setting initial properties
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Sport { get; set; }
        public int CoachId { get; set; }
        public Coach Coach { get; set; }
        public ICollection<SponsorAthlete> sponsorAthletes { get; set; }
    }
}
