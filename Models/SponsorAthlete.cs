﻿using Competition_Manager.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    public class SponsorAthlete
    {
        public int SponsorId { get; set; }
        public Sponsor sponsor { get; set; }
        public int AthleteId { get; set; }
        public Athlete athlete { get; set; }
    }
}

